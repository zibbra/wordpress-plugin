<?php

require 'recipe/common.php';

env('root_path', realpath(__DIR__ . '/../'));
env('plugin_path', __DIR__);
env('release_path', realpath(__DIR__ . '/../') . '/release');

task('deploy:version', [
	'deploy:get-version',
	'deploy:check-version'
]);

task('deploy:get-version', function() {

	$contents = file_get_contents(env('plugin_path') . '/readme.txt');
	$matches = [];

	preg_match("/Stable tag: ([0-9\.]+)/", $contents, $matches);

	env("version", $matches[1]);

}); // end deploy:get-version

task('deploy:check-version', function() {

	$folder = env('release_path') . '/tags/' . env('version');

	if(is_dir($folder)) {

		writeln('<error>Version ' . env('version') . ' is already published!</error>');
		exit;

	} // end if

}); // end deploy:check-version

task('deploy:svn', [
	'deploy:svn-trunk',
	'deploy:svn-create-tag',
	'deploy:svn-commit'
]);

task('deploy:svn-trunk', function() {

	$appFiles = [
		'core',
		'css',
		'images',
		'jscripts',
		'languages',
		'modules',
		'tags',
		'templates',
		'widgets',
		'readme.txt',
		'zibbra.php'
	];

	foreach ($appFiles as $file)
	{
		writeln('Copying <info>' . $file . '</info> to the trunk folder...');
		runLocally('cp -R ' . env('plugin_path') . '/' . $file . ' ' . env('release_path') . '/trunk/');

	} // end foreach

}); // end deploy:svn-trunk

task('deploy:svn-create-tag', function() {

	writeln('Creating tag <info>' . env('version') . '</info>...');
	runLocally('cd ' . env('release_path') . ' && svn cp trunk tags/' . env('version'));

}); // end deploy:svn-create-tag

task('deploy:svn-commit', function() {

	writeln('Committing to Wordpress SVN...');
	runLocally('cd ' . env('release_path') . ' && svn commit -m "Release version ' . env('version') . '"');

}); // end deploy:svn-commit

task('deploy', [
	'deploy:version',
	'deploy:svn'
]);